import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DestinosApiClienteComponent } from './destinos-api-cliente.component';

describe('DestinosApiClienteComponent', () => {
  let component: DestinosApiClienteComponent;
  let fixture: ComponentFixture<DestinosApiClienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DestinosApiClienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DestinosApiClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
